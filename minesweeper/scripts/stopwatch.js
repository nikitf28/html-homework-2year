
var sec = 0;
var min = 0;
var t;
var  sw;

function initSW(){
  sw = document.getElementsByClassName('stopwatch')[0];
}
function tick(){
    sec++;
    if (sec >= 60) {
        sec = 0;
        min++;
    }
}
function add() {
    tick();
    sw.textContent = (min > 9 ? min : "0" + min)
       		 + ":" + (sec > 9 ? sec : "0" + sec);
    timer();
}
function timer() {
    t = setTimeout(add, 1000);
}

function stop_timer(){
  clearTimeout(t);
}

function reset_timer(){
  stop_timer();
  sec = 0;
  min = 0;
  sw.textContent = "00:00";
}

document.addEventListener("DOMContentLoaded", initSW);
