let tasks = [
  {'done': true, 'high_priority': false, 'discription': 'Создать HTML файл'},
  {'done': true, 'high_priority': false, 'discription': 'Задать CSS свойства'},
  {'done': true, 'high_priority': false, 'discription': 'Добавить код на JavaScript'},
  {'done': false, 'high_priority': false, 'discription': 'Простетироать всё'},
  {'done': false, 'high_priority': false, 'discription': 'Запушить в Git'},
  {'done': false, 'high_priority': false, 'discription': 'Связаться с заказчиком, выверить правки, согласовать оплату, всё это до 10 декабря. Zoom_id: 859 458 965. Password: dasdasd.'},
  {'done': false, 'high_priority': false, 'discription': 'Купить билеты на море.'},
  {'done': true, 'high_priority': true, 'discription': 'Покормить кота в 0:00'},
  {'done': true, 'high_priority': true, 'discription': 'Покормить кота в 1:00'},
  {'done': true, 'high_priority': true, 'discription': 'Покормить кота в 2:00'},
  {'done': true, 'high_priority': true, 'discription': 'Покормить кота в 3:00'},
  {'done': true, 'high_priority': true, 'discription': 'Покормить кота в 4:00'},
  {'done': true, 'high_priority': true, 'discription': 'Покормить кота в 5:00'},
  {'done': true, 'high_priority': true, 'discription': 'Покормить кота в 6:00'},
  {'done': false, 'high_priority': true, 'discription': 'Покормить кота в 7:00'},
  {'done': false, 'high_priority': true, 'discription': 'Покормить кота в 8:00'},
  {'done': false, 'high_priority': true, 'discription': 'Покормить кота в 9:00'},
  {'done': false, 'high_priority': true, 'discription': 'Покормить кота в 10:00'},
  {'done': false, 'high_priority': true, 'discription': 'Покормить кота в 11:00'},
  {'done': false, 'high_priority': true, 'discription': 'Покормить кота в 12:00'},
  {'done': false, 'high_priority': true, 'discription': 'Покормить кота в 13:00'},
  {'done': true, 'high_priority': true, 'discription': 'Покормить кота в 14:00'},
  {'done': true, 'high_priority': true, 'discription': 'Покормить кота в 15:00'},
  {'done': true, 'high_priority': true, 'discription': 'Покормить кота в 16:00'},
  {'done': false, 'high_priority': true, 'discription': 'Покормить кота в 17:00'},
  {'done': false, 'high_priority': true, 'discription': 'Покормить кота в 18:00'},
  {'done': false, 'high_priority': true, 'discription': 'Покормить кота в 19:00'},
  {'done': true, 'high_priority': true, 'discription': 'Покормить кота в 20:00'},
  {'done': true, 'high_priority': true, 'discription': 'Покормить кота в 21:00'},
  {'done': false, 'high_priority': true, 'discription': 'Покормить кота в 22:00'},
  {'done': true, 'high_priority': true, 'discription': 'Покормить кота в 23:00'},
]

let card_task_id = -1;

function show_task_card(show, task_id){
  card_task_id = task_id;
  let task_card = document.getElementsByClassName('task_card')[0];
  if (show){
    task_card.classList.remove('task_cards_hide');
  }
  else{
    task_card.classList.add('task_cards_hide');
  }

  if (task_id != -1){
    let task_card = document.getElementsByClassName('task_card')[0];
    let task_discription = tasks[id].discription;
    task_card.getElementsByClassName('task_card_input')[0].value = task_discription;
    if (tasks[task_id].high_priority){
      task_card.getElementsByClassName('checkbox')[0].classList.add('checkbox_checked');
    }
    else {
      task_card.getElementsByClassName('checkbox')[0].classList.remove('checkbox_checked');
    }
  }
  else{
    task_card.getElementsByClassName('task_card_input')[0].value = '';
    task_card.getElementsByClassName('checkbox')[0].classList.remove('checkbox_checked');
  }
}

function checkbox_handler(){
  let state;
  if (this.classList.contains('checkbox_checked')){
    this.classList.remove('checkbox_checked');
    state = false;
  }
  else{
    this.classList.add('checkbox_checked');
    state = true;
  }

  if (this.classList.contains('task_list_task_checkbox')){
    parent = this.parentElement;
    id = parent.id;
    tasks[id].done = state;
    init();
  }
}

function trash_handler(){
  parent = this.parentElement;
  id = parent.id;
  tasks.splice(id, 1);
  init();
}

function pencil_handler(){
  parent = this.parentElement;
  id = parent.id;
  show_task_card(true, id);
}

function load_tasks(){
  let html_tasks_list = document.getElementsByClassName('task_list_tasks')[0];
  let html_tasks =  document.getElementsByClassName('task_list_task');
  html_tasks_list.innerHTML = '';

  for (var i = 0; i < tasks.length; i++){
    let div = document.createElement('div');
    div.id = i;
    div.classList.add('task_list_task');

    let checkbox = document.createElement('div');
    checkbox.classList.add('checkbox');
    checkbox.classList.add('task_list_task_checkbox');

    if (tasks[i].done){
      checkbox.classList.add('checkbox_checked');
    }

    let high_priority = document.createElement('div');
    high_priority.classList.add('flash');
    high_priority.classList.add('task_list_task_flash');
    if (!tasks[i].high_priority){
      high_priority.classList.add('task_list_task_no_priority');
    }

    let discription = document.createElement('div');
    discription.classList.add('task_list_task_discription');
    discription.textContent = tasks[i].discription;
    if (tasks[i].done){
        discription.classList.add('task_list_task_discription_completed');
    }

    let trash = document.createElement('div');
    trash.classList.add('trash');
    trash.classList.add('task_list_task_trash');
    if (tasks[i].done){
        trash.classList.add('task_list_task_icon_completed');
    }

    let pencil = document.createElement('div');
    pencil.classList.add('pencil');
    pencil.classList.add('task_list_task_pencil');
    if (tasks[i].done){
        pencil.classList.add('task_list_task_icon_completed');
    }

    div.append(checkbox);
    div.append(high_priority);
    div.append(discription);
    div.append(trash);
    div.append(pencil);
    html_tasks_list.append(div);
  }
}

function save_handler(){
  let task_card = document.getElementsByClassName('task_card')[0];

  if (card_task_id != -1){
    tasks[card_task_id].discription = task_card.getElementsByClassName('task_card_input')[0].value;
    tasks[card_task_id].high_priority = task_card.getElementsByClassName('checkbox')[0].classList.contains('checkbox_checked');
  } else{
    task = {
      'done': false,
      'high_priority': task_card.getElementsByClassName('checkbox')[0].classList.contains('checkbox_checked'),
      'discription': task_card.getElementsByClassName('task_card_input')[0].value
    };
    tasks.push(task);
  }

  show_task_card(false, -1);
  init();
}

function init(){
  let add_btn = document.getElementsByClassName('task_list_add_btn')[0];
  add_btn.addEventListener('click', function(){ show_task_card(true, -1); });
  let cancel_btn = document.getElementsByClassName('task_card_cancel_btn')[0];
  cancel_btn.addEventListener('click',  function(){ show_task_card(false, -1); });

  load_tasks();

  let checkbox_list = document.getElementsByClassName('checkbox');
  for (var i = 0; i < checkbox_list.length; i++){
    checkbox_list[i].addEventListener('click', checkbox_handler);
  }

  let trash_list = document.getElementsByClassName('task_list_task_trash');
  for (var i = 0; i < trash_list.length; i++){
    trash_list[i].addEventListener('click', trash_handler);
  }

  let pencil_list = document.getElementsByClassName('task_list_task_pencil');
  for (var i = 0; i < pencil_list.length; i++){
    pencil_list[i].addEventListener('click', pencil_handler);
  }

  let save_btn = document.getElementsByClassName('task_card_save_btn')[0];
  save_btn.addEventListener('click', save_handler);
}

document.addEventListener("DOMContentLoaded", init);
