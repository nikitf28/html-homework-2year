let fio_color_save = "#202020";
let pos_color_save = "#474747";
let fio_align_save = "center";
let pos_align_save = "center";
let fio_font_save = "26px";
let pos_font_save = "16px";
let second_phone_save = false;

let organisation_input;
let name_input;
let position_input;
let phone_input;
let second_phone_input;
let email_input;
let location_input;
let email_checkbox;
let adress_checkbox;
let org_name;
let fio;
let position;
let phone;
let second_phone;
let email;
let address;

let fio_colors;
let position_colors;
let fio_align;
let pos_align;
let fio_font;
let pos_font;

let phone_plus;
let phone_minus;
let phone_notePlus;
let phone_noteMinus;
let phone_second;

function put_data(){


  org_name.innerText = organisation_input.value;
  fio.innerText = name_input.value;
  fio.style.color = fio_color_save;
  fio.style.textAlign = fio_align_save;
  fio.style.fontSize = fio_font_save;
  position.innerText = position_input.value;
  position.style.color = pos_color_save;
  position.style.textAlign = pos_align_save;
  position.style.fontSize = pos_font_save;
  phone.innerText = phone_input.value;
  email.innerText = email_input.value;
  address.innerText = location_input.value;

  if (second_phone_save){
    second_phone.innerText = second_phone_input.value;
    second_phone.style.display = "block";
  }
  else{
    second_phone.style.display = "none";
  }

  if (email_checkbox.checked){
    email.style.display = "block";
  }
  else{
    email.style.display = "none";
  }

  if (adress_checkbox.checked){
    address.style.display = "block";
  }
  else{
    address.style.display = "none";
  }
}

function fio_color_click(){
  for (var i = 0; i < fio_colors.length; i++){
    fio_colors[i].classList.remove('white_ball');
  }
  this.getElementsByTagName('div')[0].classList.add('white_ball');
  fio_color_save = getComputedStyle(this).getPropertyValue('background-color');
}

function pos_color_click(){
  for (var i = 0; i < position_colors.length; i++){
    position_colors[i].classList.remove('white_ball');
  }
  this.getElementsByTagName('div')[0].classList.add('white_ball');
  pos_color_save = getComputedStyle(this).getPropertyValue('background-color');
}

function fio_align_click(){
  for (var i = 0; i < fio_align.length; i++){
    fio_align[i].classList.remove('pressed');
  }
  this.classList.add('pressed');
  fio_align_save = this.getElementsByTagName('img')[0].alt;
}

function pos_align_click(){
  for (var i = 0; i < pos_align.length; i++){
    pos_align[i].classList.remove('pressed');
  }
  this.classList.add('pressed');
  pos_align_save = this.getElementsByTagName('img')[0].alt;
}

function fio_font_click(){
  for (var i = 0; i < fio_font.length; i++){
    fio_font[i].classList.remove('pressed');
  }
  this.classList.add('pressed');
  fio_font_save = this.innerText;
}

function pos_font_click(){
  for (var i = 0; i < pos_font.length; i++){
    pos_font[i].classList.remove('pressed');
  }
  this.classList.add('pressed');
  pos_font_save = this.innerText;
}

function phone_click(){
  if (this.alt === "plus"){
    phone_plus.style.display = "none";
    phone_minus.style.display = "block";
    phone_notePlus.style.display = "none";
    phone_noteMinus.style.display = "block";
    phone_second.style.display = "block";
    second_phone_save = true;
  }
  else{
    phone_plus.style.display = "block";
    phone_minus.style.display = "none";
    phone_notePlus.style.display = "block";
    phone_noteMinus.style.display = "none";
    phone_second.style.display = "none";
    second_phone_save = false;
  }
}

function init_buttons(){
  let phone = document.getElementById('phone_line').getElementsByTagName('img');

  for (var i = 0; i < fio_colors.length; i++){
    fio_colors[i].addEventListener('click', fio_color_click);
  }
  for (var i = 0; i < position_colors.length; i++){
    position_colors[i].addEventListener('click', pos_color_click);
  }
  for (var i = 0; i < fio_align.length; i++){
    fio_align[i].addEventListener('click', fio_align_click);
  }
  for (var i = 0; i < pos_align.length; i++){
    pos_align[i].addEventListener('click', pos_align_click);
  }
  for (var i = 0; i < fio_font.length; i++){
    fio_font[i].addEventListener('click', fio_font_click);
  }
  for (var i = 0; i < pos_font.length; i++){
    pos_font[i].addEventListener('click', pos_font_click);
  }
  for (var i = 0; i < phone.length; i++){
    phone[i].addEventListener('click', phone_click);
  }
}

function init_vars() {
  organisation_input = document.getElementById('organisation-input');
  name_input = document.getElementById('name-input');
  position_input = document.getElementById('position-input');
  phone_input = document.getElementById('phone-input');
  second_phone_input = document.getElementById('second-phone-input');
  email_input = document.getElementById('email-input');
  location_input = document.getElementById('location-input');
  email_checkbox =  document.getElementById('email_line').getElementsByClassName('checkbox')[0];
  adress_checkbox =  document.getElementById('location_line').getElementsByClassName('checkbox')[0];

  org_name = document.getElementsByClassName('org_name')[0];
  fio = document.getElementsByClassName('fio')[0];
  position = document.getElementsByClassName('position')[0];
  phone = document.getElementsByClassName('phone')[0];
  second_phone = document.getElementsByClassName('phone')[1];
  email = document.getElementsByClassName('email')[0];
  address = document.getElementsByClassName('address')[0];

  fio_colors = document.getElementById('fio-color').getElementsByClassName('color-ball');
  for (var i = 0; i < fio_colors.length; i++){
    fio_colors[i] = fio_colors[i].getElementsByTagName('div')[0];
  }
  position_colors = document.getElementById('position-color').getElementsByClassName('color-ball');
  for (var i = 0; i < position_colors.length; i++){
    position_colors[i] = position_colors[i].getElementsByTagName('div')[0];
  }

  fio_align = document.getElementById('fio-align').getElementsByClassName('button');
  pos_align = document.getElementById('pos-align').getElementsByClassName('button');
  fio_font = document.getElementById('fio-font').getElementsByClassName('button');
  pos_font = document.getElementById('pos-font').getElementsByClassName('button');

  phone_plus = document.getElementById('plus');
  phone_minus = document.getElementById('minus');
  phone_notePlus = document.getElementById('notePlus');
  phone_noteMinus = document.getElementById('noteMinus');
  phone_second = document.getElementById('second-phone-input');
}

function init(){
  init_vars();
  init_buttons();
  put_data();
  let btn = document.getElementsByClassName('btn')[0];
  btn.addEventListener('click', put_data);
}

document.addEventListener("DOMContentLoaded", init);
