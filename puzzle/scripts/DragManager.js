var DragManager = new function() {
  var dragObject = false;
  var self = this;

  function onMouseDown(e) {

    if (e.which != 1) return;


    var elem = e.target.closest('.draggable');
    if (!elem) return;
    dragObject = elem;
    dragObject.started = false;
    dragObject.downX = e.pageX;
    dragObject.downY = e.pageY;
    dragObject.style.zIndex = 999;
    return false;
  }

  function onMouseMove(e) {
    if (!dragObject) return;
    if (!dragObject.started) {

      var moveX = e.pageX - dragObject.downX;
      var moveY = e.pageY - dragObject.downY;


      if (Math.abs(moveX) < 3 && Math.abs(moveY) < 3) {
        return;
      }
      dragObject.started = true;
      if (!dragObject) {
        dragObject = false;
        return;
      }

      var coords = getCoords(dragObject);

      dragObject.shiftX = dragObject.downX - coords.left;
      dragObject.shiftY = dragObject.downY - coords.top;
      dragObject.old = {};
      dragObject.old.left = dragObject.style.left;
      dragObject.old.top = dragObject.style.top;
      //console.log("OLD " + dragObject.old.left);
    }


    dragObject.style.left = e.pageX - dragObject.shiftX + 'px';
    dragObject.style.top = e.pageY - dragObject.shiftY + 'px';

    return false;
  }

  function onMouseUp(e) {
    if (dragObject) {

      let field = document.getElementsByClassName('field')[0];
      let fieldCoords = getCoords(field);
      let fieldWidth = field.getBoundingClientRect().width;
      let fieldHeight = field.getBoundingClientRect().height;
      let insideTop = e.pageY - fieldCoords.top;
      let insideLeft = e.pageX - fieldCoords.left;
      //console.log(fieldCoords);
      if (insideTop >= 0 && insideLeft >= 0 && insideTop < fieldHeight && insideLeft < fieldWidth){
        dragObject.style.top = fieldCoords.top + insideTop - insideTop%50 + "px";
        dragObject.style.left = fieldCoords.left + insideLeft - insideLeft%50 + "px";
        //console.log(fieldCoords.width);
        let holderId = parseInt(insideTop/50)*9 + parseInt(insideLeft/50);
        //
        if (!isOk(holderId) || (EASY_MODE && holderId != dragObject.num)){
          dragObject.style.top = dragObject.old.top;
          dragObject.style.left = dragObject.old.left;
        }
      }
      dragObject.style.zIndex = 1;
      checkAll();
    }
    dragObject = false;
    dragObject.started = false;
  }

  document.onmousemove = onMouseMove;
  document.onmouseup = onMouseUp;
  document.onmousedown = onMouseDown;
};


function getCoords(elem) { // кроме IE8-
  var box = elem.getBoundingClientRect();

  return {
    top: box.top + pageYOffset,
    left: box.left + pageXOffset
  };

}
