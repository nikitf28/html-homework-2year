let cells = [];
let holders = [];
let EASY_MODE = false;

function isOk(holderId){
  return !holders[holderId];
}

function finish_game(){
  document.getElementById('winning').style.display = "block";
  let pices = document.getElementsByClassName('pic');
  for (var i = 0; i < pices.length; i++){
    pices[i].classList.remove('draggable');
  }
}

function checkAll(){
  let field = document.getElementsByClassName('field')[0];
  let fieldCoords = getCoords(field);
  let fieldWidth = field.getBoundingClientRect().width;
  let fieldHeight = field.getBoundingClientRect().height;

  let pices = document.getElementsByClassName('pic');
  for (var i = 0; i < 54; i++){
    holders[i] = false;
  }
  for (var i = 0; i < pices.length; i++){
    //let picCoords = getCoords(pices[i]);
    let insideTop =  parseInt(pices[i].style.top, 10) - fieldCoords.top;
    let insideLeft = parseInt(pices[i].style.left, 10) - fieldCoords.left;
    //console.log(fieldCoords.top + " " + fieldCoords.left);
    //console.log(insideTop + " " + insideLeft);
    //console.log((insideTop >= 0) + " " + (insideLeft >= 0) + " " + (insideTop < fieldHeight) + " " + (insideLeft < fieldWidth));
    //console.log(insideTop + " " + insideLeft);
    if (insideTop >= 0 && insideLeft >= 0 && insideTop < fieldHeight && insideLeft < fieldWidth){
      if (insideTop%50 == 0 || insideLeft%50 == 0){
        let holderId = parseInt(insideTop/50)*9 + parseInt(insideLeft/50);
        //console.log("HolderID " + holderId);
        holders[holderId] = pices[i].num;
        //console.log(pices[i]);
      }
    }
  }
  for (var i = 0; i < pices.length; i++){
    if (holders[i] != i){
      break;
    }
    if (i + 1 == pices.length){
      finish_game();
    }
  }
  //console.log(holders);
}

function put_cells(){
  //let body = document.getElementsByNameTag('b')[0];
  for (var i = 0; i < cells.length; i++){
    document.body.append(cells[i]);
    cells[i].style.top = parseInt(Math.random() * 450, 10) + "px";
    cells[i].style.left = parseInt(Math.random() * 420, 10) + "px";
    cells[i].num = i;
    //console.log(cells[i].style.top);
  }
}

function finput_handler(evt){
  if(event.target.files.length < 0){
    return;
  }
  let img = document.getElementById('preview_image');
  var reader  = new FileReader();
  let data = URL.createObjectURL(event.target.files[0]);
  //console.log(data);
  img.src = data;
  img.onload = function(){
    let img = document.getElementById('preview_image');
    let width = img.naturalWidth;
    let height = img.naturalHeight;
    img.style.display = "none";
    let startWidth = 0;
    let startHeight = 0;
    if (width/height > 3/2){
      startWidth = Math.abs(width - (height / 2 * 3)) / 2;
    }
    else{
      startHeight = Math.abs(height - (width / 3 * 2)) / 2;
    }
    //let wrapper = document.getElementById('wrapper');
    let size = (height-startHeight*2)/6;
    //wrapper.style.gridTemplateColumns = "repeat(9, " + 50 + "px)";
    //wrapper.style.gridTemplateRows = "repeat(6, " + 50 + "px)";

    //console.log(height);
    //console.log(startHeight + " " + (height - startHeight) + " " + size);
    for (var j = startHeight; j < height - startHeight; j+=size){
      for (var i = startWidth; i < width - startWidth; i+=size){
        let pic = document.createElement('div');
        pic.classList.add('pic');
        pic.style.backgroundImage = "url(" + data +  ")";
        pic.style.backgroundPosition = "right " +  (i + size) * (50 / size) + "px bottom " + (j + size) * (50 / size) + "px";
        //pic.width = size + "px";
        //pic.height = size + "px";
        pic.width = "50px";
        pic.height = "50px";

        pic.style.backgroundSize = 900 + "%";

        //console.log(pic.id + " " + pic.style.backgroundPosition);
        //wrapper.appendChild(pic);
        pic.classList.add('draggable');
        cells.push(pic);
      }
    }
    put_cells();
    document.getElementsByClassName('setups')[0].style.display = "none";
    EASY_MODE = document.getElementById('easy').checked;
  }

}

function init(){
  let finput = document.getElementById('uploaded_image');
  finput.addEventListener('change', finput_handler);
  let field = document.getElementsByClassName('field')[0];
  for (var i = 0; i < 54; i++){
    let newDiv = document.createElement('div');
    newDiv.classList.add('puzzleCell');
    field.append(newDiv);
    holders.push(false);
  }


}

document.addEventListener("DOMContentLoaded", init);
